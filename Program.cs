﻿using System;

namespace laboratorio4
{
    class Program
    {
        static void Main(string[] args)
        {
            Circulo circ1 = new Circulo();
            Console.WriteLine(circ1.ToString());
            Circulo circ2 = new Circulo();
            Circulo circ3 = circ1;
            Console.WriteLine(circ1.Equals(circ2));
            Console.WriteLine(circ1.Equals(circ3));
            Circulo circ4 = circ1.Copiar();
        }
    }
}
