namespace laboratorio4
{
    public class CirculoColorido : Circulo
    {
        private string minhaCor;
        public string Cor
        {
            get {return minhaCor;}
            set {minhaCor = value;}
        }
        public CirculoColorido()
        {
            minhaCor = "preto";
        }
        public CirculoColorido(double x, double y, double r, string c) : base(x,y,r)
        {
            minhaCor = c;
        }
        public override string ToString()
        {
            return base.ToString() + $" cor={Cor}";
        }
        public override bool Equals(object obj)
        {
            CirculoColorido outro = obj as CirculoColorido;
            if (outro == null)
            {
                return false;
            }
            else
            {
                return base.Equals((Circulo)obj) && minhaCor.Equals(outro.minhaCor);
            }
        }
    }
}