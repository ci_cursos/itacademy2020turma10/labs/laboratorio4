namespace laboratorio4
{
    public class Circulo
    {
        private double coordX;
        private double coordY;
        private double raio;
        public Circulo(double x, double y, double r)
        {
            coordX = x;
            coordY = y;
            raio = r;
        }
        public Circulo() : this(0,0,1)
        {
        }
        public double CentroX
        {
            get {return coordX;}
        }
        public double CentroY
        {
            get {return coordY;}
        }
        public double Raio
        {
            get {return raio;}
        }
        public override string ToString()
        {
            return $"({CentroX};{CentroY}) raio={Raio}";
        }
        public override bool Equals(object obj)
        {
            if ((obj == null) || !(this.GetType().Equals(obj.GetType())))
            {
                return false;
            }
            else
            {
                Circulo outro = (Circulo)obj;
                return (coordX == outro.coordX) && (coordY == outro.coordY)
                    && (raio == outro.raio);
            }
        }
        public override int GetHashCode()
        {
            return coordX.GetHashCode() ^ coordY.GetHashCode() ^ raio.GetHashCode();
        }
        public Circulo Copiar()
        {
            Circulo copia = (Circulo)this.MemberwiseClone();
            return copia;
        }
    }
}